# README
This is a resin deployment repo of the [Shongololo](https://gitlab.com/UVMGHG/shongololo) module for use on Pi3 and Pi3B+ customised for use as a data logger with CO2Meter [K30](https://www.co2meter.com/products/k-30-fs-10-000ppm-co2-sensor) and [ImetXQ](http://www.intermetsystems.com/products/imet-xq) sensors.

Note:  In this deployment the ImetXQ GPS is required for both correct UTC time stamps and geotags.  The system will operate fine without a ImetXQ but time stamps will be based irroneously on the time at which the image was created rather than real world time and there will be no GPS co-ordinates associated with recorded data.

## Deployment
Deploying takes 2 steps: (1)SD Card createion, (2) Instalation.  2 methods of SDCard creation are described below.  Both will use the same installation instructions following.

### Method 1. SDCard creaion by forking this repository.
Fork this repo, clone the result to a computer, and create your own [resin.io](https://resin.io/) application.  Copy the application's repo url and set it as your new forked and cloned repo's remote resin master 
```bash
$ cd resin_shongololo/
$ git add remote resin master <path to your resin application repo>
```
Add a device to your resin.io application and follow the steps for installation below.

### Method 2. SDCard creation by using a supplied existing resinos image
The following process is a once off requirement.  Once complete your system will automatically update whenever it is connected to the internet and an update is waiting.
1. Write the image to an SD card.  [Etcher](https://etcher.io/) is a easy to use cross platform tool for doing this.  Install etcher, select the supplied image, select the destination SD card, and click flash.  Wait until Etecher saying flashing complete.
    *WARNING: In Windows once etcher has completed flashing the image it will tell you your card needs formatting, DO NOT let it format it, just press cancle*
2. Next you need to edit and copy the network configuration file [ShongololoX](https://gitlab.com/UVMGHG/shongololo_resin/blob/master/ShongololoX) to the correct location on the SD card.
    2.1 Change the name of the configuration file to the name you want the data logger's wifi to hav (NEW_NAME)e.  If you intend to operate multiple data loggers simultanously it is advisable to name them uniquely eg: VTAG0, VTAG1, VTAG2....
    2.2 Open the network configuration file and change 3 things:
    * id=NEW_NAME
    * ssid=NEW_NAME
    * psk=PICK_A_PASSWORD
    2.3 After writing the image to the SD card it will have 4 partitions, copy the new network configuration file into the partion: resin-boot/system-connections/.  
3. Safely eject the SD card and insert into your Pi and power it on with at 3A, 5V supply

### Installation
1. Connect the ethernet port of your powered up Pi loaded with a valid resin OS image on a SDCard. 
2. Connect the pi to your computer via the ethernet port and configure your computer's wifi connection to share it's internet connection with the ethernet connection to the Pi.  
* **Windows10:** There is a short guide on how to do this in windows [here](http://www.informit.com/articles/article.aspx?p=2455390&seqNum=6)
* **Linux:** If your distribution is using network manager from the command line run
'''bash
$ nm-connection-editor
And add a connection, select type ethernet, create, then on tab IPv4 Settings select Method Shared to other computers. That should be all for connection sharing.
3. Once the Pi is online go makes some tea and give it 10min to automatically download and install the application.  If there are updates in the future this is all you will need to do to get the updates loaded - just connect your pi to an internet connection and it will automatically update.  
If you know how to see the Pi's ethernet IP on your operating system you can go put that IP in a browser and on port 5000 the application will be visible once installation has completed.  If you don't know how to do this just wait 10min to garuntee the system has fully configured and move on to the next step.
4. You have now successfully configured your Shongololo CO2 monitoring Ground Station.  If you created your own image (Method1) in your own resin.io account you will also be able to see the device as connected to the internet and uptodate in their interface.  Either way you should now disconnect the ethernet port, and disconnect your computer from your local wifi network and connect it to the Pi's access point, this will be named whatever you used for NEW_NAME when you configured the network and the password will be whatever you used for PICK_A_PASSWORD.
5. Once you've connected your computer to the Pi's wifi networrk open a browser an type into the url bar: 10.41.1.1:5000 and the application will be visible
6. See instructions here on how to use the [Ground Station](https://gitlab.com/UVMGHG/shongololo_resin/blob/master/GroundStationUse.md)

## Maintenance
Once deployed as above whenever the Pi is powered on and connected to an internet connection via its ethernet port it will automatically update if there are updates and you are either using a supplied image or have updated your forked repository

## Development
Shongololo is it's own python package managed in this [repo](git@gitlab.com:UVMGHG/shongololo.git) and published in pypy.  All changes to it should be contributed there.

If changes are made to this resin deployment instance of Shongololo as a CO2 ground station submit PRs here. 

If you just want to make changes in your deployment, you must host your own fork of this repo and generate your own resin os application and device images.  Once created updating devices is as simple as:
```bash
# git push resin master
```
