# Use in the field
1. Connect to Pi Wifi 

2. Open a browser and go to: 10.42.1.1:5000

3. Click "Setup Logger" and wait for logs to say:
        “ Finished sensor test sequence.
           Closed Sensors”
	Notes: 
		[i] This may take a couple seconds to start the 1st time.
	    	[ii] If anything fails and sensors are already plugged in press "Shutdown Logger" and   
                               Setup Logger" again.

4. When ready to capture data click: "Start Data Capture".  
	Notes:
		This can take a couple seconds to get going.

5. When ready to stop capture click: "Stop Data Capture"

6. Repeat (4-5) as many times as required

7. To download data collected click "Download Files"

8. When done in the field click "Shutdown Logger" followed by "Shutdown Pi3"
